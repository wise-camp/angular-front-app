import { Timestamp } from 'rxjs';

export class Session {
    id_session: number;
    periode: Timestamp<any>;
    adresse_inscription: string;
    modalites_inscription: string;
    periode_inscription: Timestamp<any>;
    etat_recrutement: string;
    id_formation: number
}