export class OrganismeFinanceur {
    id_organisme_financeur: number;
    code_financeur: string;
    nb_places_financees: number;
    SIRET_formateur: number;
    raison_sociale_formateur: string;
    contact_formateur: string;
    potentiel: string
}