import { Partenaire } from './partenaire';
import { Token } from './token';

export class User {
    public id: number;
    public mail: string;
    public nom?: string;
    public admin: boolean;
    public encryptedPassword?: string;
    public partenaire?: Partenaire;
    public token?: Token;

}