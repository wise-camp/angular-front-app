export class OrganismeFormation {

    id_organisme_de_formation_responsable: number;
    numero_activite: number;
    siret_organisme_formation: number;
    nom_organisme: string;
    raison_sociale: string;
    coordonnees_organisme: string;
    contact_organisme: string;
    renseignements_specifiques: string;
    potentiel: number;
    agreement_datadock: string
    
}