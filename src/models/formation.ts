export class Formation {

    id_formation: number;
    domaine_formation: string;
    numberitule_formation: string;
    objectif_formation: string;
    resultats_attendus: string;
    contenu_formation: string;
    certifiante: string;
    contact_formation: string;
    parcours_de_formation: string;
    code_niveau_entree: string;
    objectif_general_formation: string;
    certification: number;
    code_niveau_sortie: number;
    url_formation: string;
    action: string;
    liens_entreprise: string;
    organisme_formation_responsable: string;
    credits_ects: number;
    identifiant_module: string;
    positionnement: string;
    sous_modules: string;
    modules_prerequis: string;
    eligibilite_cpf: string;
    validations: string;
    id_offres: number
}