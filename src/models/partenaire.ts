import { OrganismeFormation } from './organismeFormation';
import { OrganismeFinanceur } from './organismeFinanceur';

export class Partenaire {

    id_partenaire: number;
    logo: string;
    nom: string;
    organisme_de_formation_responsable: OrganismeFormation;
    organisme_financement: OrganismeFinanceur

}