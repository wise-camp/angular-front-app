import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpUserEvent } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';
import { LieuFormation } from 'src/models/lieuFormation';

@Injectable({
    providedIn: 'root'
})

export class FormationService {

    constructor(
        private _httpClient: HttpClient
    ) {
        const user = JSON.parse(localStorage.getItem("user")) || [];
    }

    getAll(criteria): LieuFormation[] | Error {
        this._httpClient.post<LieuFormation[]>(environment.apiUrl + "formations/getAll", criteria)
        .subscribe(
            formations => {
                if (formations.length > 0)
                    return formations;
                else
                    return new Error("Pas de formations trouvées pour ces critères");
            },
            (error) => {
                return error;
            }
        );
        return new Error("erreur requête formations");
    }

    getSingle(id: number) {
        this._httpClient.post<LieuFormation[]>(environment.apiUrl + "formations/getSingle", id)
        .subscribe(
            formation => { 
                return formation;
            },
            (error) => {
                return error;
            }
        );
        return new Error("erreur requête formation");
    }

}