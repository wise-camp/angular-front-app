import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Md5 } from 'ts-md5';
import { HttpClient, HttpResponse, HttpUserEvent } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';
import { OrganismeFormation } from '../models/organismeFormation';

@Injectable({
    providedIn: 'root'
})

export class LoginService {
    user: User;

    constructor(
        private _httpClient: HttpClient
    ) {
        const user = JSON.parse(localStorage.getItem("user")) || [];
    }

    login(username: string, password: string): User | Error {

        this._httpClient.post<User>(environment.apiUrl + "login", {
            username: username, encryptedPassword: Md5.hashStr(password)
        }).subscribe(
            user => {
                return user;
            },
            (error) => {
                return error;
            }
        );
        return new Error("login request failed");
    }

    register(email: string): Observable<any> {
        return this._httpClient.post<any>(environment.apiUrl + "register", {
            email: email
        });
    }

    logout() {

    }

}