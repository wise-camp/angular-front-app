import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { SearchComponent } from './search/search.component';
import {
	MatToolbarModule,
	MatButtonModule,
	MatGridListModule,
	MatFormFieldModule,
	MatInputModule,
	MatTabsModule,
	MatIconModule
} from '@angular/material';
import { NavbarComponent } from './navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { LoginService } from 'src/services/login.service';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
	declarations: [
		AppComponent,
		LoginComponent,
		SearchComponent,
		NavbarComponent,
		HomeComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		MatToolbarModule,
		BrowserAnimationsModule,
		MatButtonModule,
		MatGridListModule,
		MatFormFieldModule,
		HttpClientModule,
		MatInputModule,
		MatTabsModule,
		MatIconModule,
		MatInputModule,
    FlexLayoutModule
	],
	providers: [
		LoginService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
