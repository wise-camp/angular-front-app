import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from 'src/services/login.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss'],
	encapsulation: ViewEncapsulation.None,
	providers: [LoginService, FormBuilder]
})
export class LoginComponent implements OnInit {

	loginForm: FormGroup;
	registerForm: FormGroup;

	constructor(
		private _formBuilder: FormBuilder,
		private _loginService: LoginService
	) {
		this.loginForm = this._formBuilder.group({
			username: ['', Validators.required],
			password: ['', Validators.required]
		});
		this.registerForm = this._formBuilder.group({
			email: ['', Validators.required]
		});
	}

	ngOnInit() {
	}

	login() {
		let loginReq = this._loginService.login(
			this.loginForm.controls["username"].value,
			this.loginForm.controls["password"].value
		);
		console.log(loginReq);
	}

	register() {
		let registerReq = this._loginService.login(
			this.loginForm.controls["username"].value,
			this.loginForm.controls["password"].value
		);
		console.log(registerReq);
	}

}
